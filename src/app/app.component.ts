import {Component} from '@angular/core';
import {Rower} from './bike.component';
import {RoweryService} from './RoweryService';

const CHANGE_BIKE_BACKGROUND = 10;

// const ROWERY: Rower[]

@Component({
    selector: "app-root",
    styleUrls: ['./app.component.css'],
    template: `
    <div id="over-header">
        <h1>{{title}}</h1>
        <h2>{{showBikes}}</h2>
    </div>
    <div id="content" >
        <div class="bikes">
            <RowerComponent *ngFor="let biker of bikes" [rower]=biker></RowerComponent>
        </div>
        <div class="countBikes() >= CHANGE_BIKE_BACKGROUND : summing-up-paragraph more-bikes-div ? summing-up-paragraph less-bikes-div">
            <span>{{bikeCountStatement}}</span>
            <span>{{countBikes()}}</span>
        </div>      
    </div>`
})

export class AppComponent {
    title = 'Rowery';
    showBikes = 'Stan sklepu:';
    bikeCountStatement = 'W sumie rowerów: ';
    noBike = 'Brak';
    bikes:Rower[];

    constructor(private roweryService:RoweryService) {
    }

    ngOnInit():void {
        this.roweryService.getBikes()
            .then(bikes => this.bikes = bikes);
    }

    countBikes():number {
        let sum = 0;

        for (let bike of this.bikes) {
            sum += bike.ilosc;
        }

        return sum;
    }
}
