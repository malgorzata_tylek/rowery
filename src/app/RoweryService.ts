import { Injectable } from '@angular/core';
import { Rower } from './bike.component';
import { BIKES } from './mockData';
// import { Http, Headers, Response } from '@angular/http';
// import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map'

@Injectable()
export class RoweryService {
  getBikes(): Promise<Rower[]> {
    return Promise.resolve(BIKES);
  }

  // getMockBikes() : Rower[] {
  //   let bikesList: Rower[];
  //
  //   this.getMockData()
  //   .subscribe((bikes) => {
  //      bikesList = bikes;
  //   });
  //
  //   debugger;
  //
  //   return bikesList;
  // }
  //
  // getMockData() : Observable<any> {
  //   let apiUrl = './mockData.json';
  //   return this.http.get(apiUrl)
  //   .map((response: Response) => {
  //     console.log(response);
  //     return response.json();
  //   });
  // }
}
