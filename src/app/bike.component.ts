import {Component, Input} from '@angular/core';
import {CurrencyPipe} from '@angular/common';

// const NO_BIKE:string = 'Brak';
// const PLUS = '+';
// const MINUS = '-';

let images:string[] = [
    "/assets/images/pink.jpg",
    "/assets/images/green.jpg",
    "/assets/images/darkblue.jpg",
    "/assets/images/red.jpg",
    "/assets/images/black.jpg",
];

export class Rower {
    id:number;
    name:string;
    description:string;
    ilosc:number;
    cena:number;
    waluta:string;
    zamowione:number;
}

@Component({
    selector: 'RowerComponent',
    styleUrls: ['./bike.component.css'],
    template: `
      <span>
        <img [src]=getImagePath() >
      </span>
      <span class="column right">
        <div class="name">{{rower.name | uppercase}}</div>
        <div>{{rower.description}}</div>
        <div>{{rower.cena | currency: rower.waluta: true}}</div>
      </span>
      <span class="column right">
        <div>
          <span>Ilość: </span>
          <span class="right" *ngIf="rower.ilosc === 0">Brak</span>
          <span class="right" *ngIf="rower.ilosc > 0">{{rower.ilosc}}</span>
        </div>
        <div>
          <span>Zamówione: </span>
          <span class="right">{{rower.zamowione}}</span>
        </div>
        <div>
          <button *ngIf="rower.ilosc > 0" (click)="more()">+</button>
          <button *ngIf="rower.zamowione > 0"(click)="less()">-</button>
        </div>
      </span>
    `
})

export class RowerComponent {
    @Input() rower:Rower;

    more() {
        this.rower.ilosc -= 1;
        this.rower.zamowione += 1;
    }

    less() {
        this.rower.ilosc += 1;
        this.rower.zamowione -= 1;
    }

    getImagePath():string {
        return images[this.rower.id - 1];
    }

}
