import { Rower } from './bike.component';

export const BIKES: Rower[] = [
  {
    id: 1,
    name: "Malina",
    description: "Różowy rower z bocznymi kółkami",
    ilosc: 7,
    cena: 200,
    waluta: "USD",
    zamowione: 2
  },
  {
    id: 2,
    name: "Kiwi",
    description: "Zielony rower",
    ilosc: 0,
    cena: 52,
    waluta: "EUR",
    zamowione: 0
  },
  {
    id: 3,
    name: "Borówka",
    description: "Granatowy rower",
    ilosc: 9,
    cena: 1289,
    waluta: "PLN",
    zamowione: 1
  },
  {
    id: 4,
    name: "Porzeczka",
    description: "Czerwony rower z koszykiem",
    ilosc: 2,
    cena: 85,
    waluta: "EUR",
    zamowione: 0
  },
  {
    id: 5,
    name: "Cukierek",
    description: "Czarny rower stacjonarny",
    ilosc: 3,
    cena: 222,
    waluta: "PLN",
    zamowione: 2
  }
]
